import React, { Component } from 'react';
import './App.css'; 
import Person from './Person/Person.js';
class App extends Component {
  state={
    persons:[
      { name:"max",age:28 },
      { name:"manu",age:48 },
      { name:"stephniie",age:8 },
      
    ],
    otherState:'some other value'
  }
  switchNameHandler = () =>{
    // console.log('was clicked');
    // this.state.persons[0].name="karthik";
    this.setState({persons:[
      { name:"a",age:11 },
      { name:"b",age:22 },
      { name:"c",age:33 },
    ]})
  }
  render() {
    const style={
backgroundColor: 'white',
font:'inherit',
border:'1px solid blue',
padding:'8px',
cursor: 'pointer'
    };
    return (
      <div className="App">
         <h1>Hi, I'm a react app</h1>
         {/* <p>this is the paragraph how is it?</p> */}
         <button
         style={style}
         onClick={this.switchNameHandler}>Switch Name</button>
         <Person
          name={this.state.persons[0].name} 
          age={this.state.persons[0].age} />
         <Person 
         name={this.state.persons[1].name} 
         age={this.state.persons[1].age} />
         <Person 
         name={this.state.persons[2].name} 
         age={this.state.persons[2].age}
         click={this.switchNameHandler} />

         <Person name="adolf" age="4">hobbies</Person>

      </div>
      // <h1> another heading</h1>
    );
    // return  React.createElement("div",null,'h1','Hi, I\'m a react app!!!')
  }
}

export default App;
